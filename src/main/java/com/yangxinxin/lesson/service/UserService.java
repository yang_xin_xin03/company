package com.yangxinxin.lesson.service;

import com.yangxinxin.lesson.entity.SysUser;
import com.yangxinxin.lesson.vo.request.LoginReqVO;
import com.yangxinxin.lesson.vo.request.UserPageReqVO;
import com.yangxinxin.lesson.vo.respond.LoginRespVO;
import com.yangxinxin.lesson.vo.respond.PageVO;

/** @Author yangxinxin
 * @Description //TODO
 * @Date 11:45 2020/3/8
 * @Param
 * @return
 **/
public interface UserService {
    /**
     * 用户登录
     * @param vo
     * @return
     */
    LoginRespVO login(LoginReqVO vo);

    /**
     * 退出登录
     * @Author:      yangxinxin
     * @UpdateUser:
     * @Version:     0.0.1
     * @param accessToken
     * @param refreshToken
     * @return       void
     * @throws
     */
    void logout(String accessToken, String refreshToken);

    PageVO<SysUser> pageInfo(UserPageReqVO vo);
}
