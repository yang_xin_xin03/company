package com.yangxinxin.lesson.shiro;

import org.apache.shiro.authc.UsernamePasswordToken;

/** @Author yangxinxin
 * @Description //TODO 
 * @Date 14:29 2020/3/8
 * @Param 
 * @return 
 **/
public class CustomUsernamePasswordToken extends UsernamePasswordToken {

    private String token;

    public CustomUsernamePasswordToken(String token) {
        this.token = token;
    }

    @Override
    public Object getPrincipal() {
        return token;
    }
}
