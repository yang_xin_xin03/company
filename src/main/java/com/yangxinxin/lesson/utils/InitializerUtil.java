package com.yangxinxin.lesson.utils;

import org.springframework.stereotype.Component;

/** @Author yangxinxin
 * @Description //TODO 
 * @Date 10:53 2020/3/8
 * @Param 
 * @return 
 **/
@Component
public class InitializerUtil {
    private TokenSettings tokenSettings;
    public InitializerUtil(TokenSettings tokenSettings){
        JwtTokenUtil.setTokenSettings(tokenSettings);
    }
}

