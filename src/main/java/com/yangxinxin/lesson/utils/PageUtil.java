package com.yangxinxin.lesson.utils;

import com.github.pagehelper.Page;
import com.yangxinxin.lesson.vo.respond.PageVO;

import java.util.List;

/** @Author yangxinxin
 * @Description //TODO
 * @Date 11:50 2020/3/8
 * @Param
 * @return
 **/
public class PageUtil {
    private PageUtil(){}
    public static <T> PageVO<T> getPageVO(List<T> list){
        PageVO<T> result=new PageVO<>();
        if(list instanceof Page){
            Page<T> page= (Page<T>) list;
            result.setTotalRows(page.getTotal());
            result.setTotalPages(page.getPages());
            result.setPageNum(page.getPageNum());
            result.setCurPageSize(page.getPageSize());
            result.setPageSize(page.size());
            result.setList(page.getResult());
        }
        return result;
    }
}
