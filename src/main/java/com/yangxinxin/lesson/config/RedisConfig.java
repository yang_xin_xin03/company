package com.yangxinxin.lesson.config;

import com.yangxinxin.lesson.serializer.MyStringRedisSerializer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.connection.RedisConnectionFactory;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.serializer.StringRedisSerializer;

/** @Author yangxinxin
 * @Description //TODO 
 * @Date 13:34 2020/3/7
 * @Param 
 * @return 
 **/
@Configuration
public class RedisConfig {

    @Bean
    public RedisTemplate<String,Object> redisTemplate(RedisConnectionFactory redisConnectionFactory){
        RedisTemplate<String,Object> redisTemplate=new RedisTemplate<>();
        //1.设置链接工厂
        redisTemplate.setConnectionFactory(redisConnectionFactory);
        //key value
        StringRedisSerializer stringRedisSerializer=new StringRedisSerializer();
        MyStringRedisSerializer myStringRedisSerializer=new MyStringRedisSerializer();
        //key的序列化
        redisTemplate.setKeySerializer(stringRedisSerializer);
        //HashKey的序列化
        redisTemplate.setHashKeySerializer(stringRedisSerializer);
        //value
        redisTemplate.setHashValueSerializer(myStringRedisSerializer);
        //value
        redisTemplate.setValueSerializer(myStringRedisSerializer);
        return redisTemplate;
    }
}
