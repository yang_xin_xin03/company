package com.yangxinxin.lesson.controller;
import com.yangxinxin.lesson.exception.BusinessException;
import com.yangxinxin.lesson.exception.code.BaseResponseCode;
import com.yangxinxin.lesson.utils.DataResult;
import com.yangxinxin.lesson.vo.request.TestReqVO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;
/** @Author yangxinxin
 * @Description //TODO 
 * @Date 12:57 2020/3/7
 * @Param 
 * @return 
 **/

/**
 * 前后端分离的模式
 */
@RestController
@RequestMapping("/test")
@Api(tags = "测试模块",description = "测试模块相关接口")
public class TestController {
    @GetMapping("/index")
    @ApiOperation(value = "测试index接口")
    public String testRestful(){
        return "Hello World!";
    }


    @GetMapping("/home")
    @ApiOperation(value = "测试统一返回格式接口")
    public DataResult getHome(){
//          DataResult result=DataResult.success();
        DataResult result=DataResult.getResult(BaseResponseCode.SUCCESS);
        //封装异常

        result.setData("哈哈哈我请求成功了");
        List<String> list = new ArrayList<>();
        list.add("a");
        list.add("b");
        result.setData(list);
        int i = 1 / 0;
        return result;
    }


    //脏数据异常处理
    @GetMapping("/business/error")
    @ApiOperation(value = "测试主动抛出业务异常接口")
    public DataResult<String> testBusinessError(@RequestParam String type){
        if(!type.equals("1")||type.equals("2")||type.equals("3")){
            throw new BusinessException(BaseResponseCode.DATA_ERROR);
        }
        DataResult<String> result=new DataResult(0,type);
        result.setData("业务正常！");
        return result;
    }

    @PostMapping("/valid/error")
    @ApiOperation(value = "测试Validator抛出业务异常接口")
    public DataResult testValid(@RequestBody @Valid TestReqVO vo){
        DataResult result=DataResult.success();
        return result;
    }



}
