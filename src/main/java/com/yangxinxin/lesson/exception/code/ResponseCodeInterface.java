package com.yangxinxin.lesson.exception.code;

/** @Author yangxinxin
 * @Description //TODO 
 * @Date 14:45 2020/3/7
 * @Param 
 * @return 
 **/
public interface ResponseCodeInterface {
    int getCode();
    String getMsg();
}
