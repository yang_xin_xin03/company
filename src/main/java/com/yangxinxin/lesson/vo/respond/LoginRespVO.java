package com.yangxinxin.lesson.vo.respond;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/** @Author yangxinxin
 * @Description //TODO 
 * @Date 11:45 2020/3/8
 * @Param 
 * @return 
 **/
@Data
public class LoginRespVO {
    @ApiModelProperty(value = "正常的业务token")
    private String accessToken;
    @ApiModelProperty(value = "刷新token")
    private String refreshToken;
    @ApiModelProperty(value = "用户id")
    private String id;
    @ApiModelProperty(value = "手机号")
    private String phone;
    @ApiModelProperty(value = "账号")
    private String username;
}
