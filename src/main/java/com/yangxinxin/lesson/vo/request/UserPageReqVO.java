package com.yangxinxin.lesson.vo.request;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/** @Author yangxinxin
 * @Description //TODO 
 * @Date 11:45 2020/3/8
 * @Param 
 * @return 
 **/
@Data
public class UserPageReqVO {

    @ApiModelProperty(value = "当前第几页")
    private Integer pageNum=1;

    @ApiModelProperty(value = "当前页数量")
    private Integer pageSize=10;
}
